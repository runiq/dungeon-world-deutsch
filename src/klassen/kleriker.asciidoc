== Der Kleriker

Die Länder der Welt sind ein gottverlassener Trümmerhaufen.
Sie sind übersät mit wandelnden Toten, Bestien jeder Art und endlosen, widernatürlichen Weiten zwischen geweihten Horten der Zuflucht.
Die Welt da draußen ist gottlos – und deswegen braucht sie dich.

Es ist nicht nur deine Natur, den Heiden die Herrlichkeit Gottes zu offenbaren – nein, es ist deine Bestimmung.
Dir obliegt es, sie mit Schwert und Streitkolben und Zauber zu bekehren;
dir obliegt es, das Herz der Wildnis zu spalten und dort den Keim der Göttlichkeit zu legen.
Es heißt, man tut gut daran, Gott im Herzen zu tragen.
Du weißt es besser:
Gott wohnt auf der Schneide einer Klinge.

Zeig der Welt, wer ihr Herr ist.

=== Namen

_Zwerg_: Durga, Aelfar, Gerda, Rurgosh, Bjorn, Drummond, Helga, Siggrun, Freya

_Mensch_: Wesley, Brinton, Jon, Sara, Hawthron, Elise, Clarke, Lenore, Piotr, Dahlia, Carmine

=== Aussehen

Wähle von jedem eins:

Gütige, scharfe oder traurige Augen

Tonsur, fremdartiger Haarputz oder Glatze

Wallende Gewänder, Mönchskutte oder gewöhnliche Kleidung

Dünner, knorriger oder wabbeliger Körper

=== Attribute

Deine maximalen TP sind 8+Konstitution.

Dein Grundschaden beträgt 1W6.

=== Anfangszüge

*Wähle eine Rasse und erlange ihren Zug:*

*Zwerg*

'''
Du bist eins mit den Stein.
Wenn du _in Gott gehst_, wird dir zusätzlich eine besondere Variante von _Worte der Stillschweigenden_ gewährt, die nur auf Stein angewandt werden kann.

'''

*Mensch*

'''
Deine Glaubensrichtungen sind mannigfaltig.
Wähle einen Magierzauber.
Er wird dir gewährt wie ein Klerikerzauber, und Du kannst ihn genauso wirken.

'''

*Du beginnst das Spiel mit den folgenden Zügen:*

*Gottheit*

'''
Du dienst und huldigst einer Gottheit oder Macht, die dir Zauber gewährt.
Gib ihr einen Namen (vielleicht Helferth, Sucellus, Zorica oder Krugon der Freudlose) und wähle ihre Domäne:

* Heilung und Wiederherstellung
* Blutige Eroberung
* Kultur und Zivilisation
* Wissen und Verborgenes
* Die Unterdrückten und Vergessenen
* Was unter uns lauert

Wähle ein Gebot, das dir auferlegt wird:

* Deine Religion predigt die Heiligkeit des Leidens – du erhältst _Ersuchen um Leiden_
* Deine Religion ist kultisch und verschlossen – du erhältst _Ersuchen um Geheimnisse_
* Deine Religion misst Opferriten eine besondere Bedeutung bei – du erhältst _Ersuchen um Opfergaben_
* Deine Religion glaubt an Rechtfindung durch Kampf – du erhältst _Ersuchen um persönlichen Triumph_

'''

*Göttliche Führung*

'''
*Wenn du deine Gottheit im Rahmen deines Gebots um etwas bittest,* wird dir eine nützliche Eingebung oder Gunst zuteil, die mit der Domäne deiner Gottheit zusammenhängt.
Der SL wird sie dir beschreiben.

'''

*Untote vertreiben*

'''
*Wenn du dein heiliges Symbol emporhältst und deine Gottheit um Schutz anrufst,* würfle+WEI.
✴ Bei einer 7+ kommt dir kein Untoter nahe, solange du betest und dein heiliges Symbol zur Schau stellst.
✴ Bei einer 10+ blendest du zudem intelligente Untote und schlägst geistlose Untote in die Flucht.
Eine aggressive Handlung deinerseits bricht den Bann und bewirkt, dass sie wieder normal handeln können.
Sei gewarnt: Intelligente Untote sind gerissen und können Mittel und Wege finden, dir auch aus der Ferne zu schaden.

'''

*In Gott gehen*

'''
*Wenn du etwa eine Stunde ununterbrochen damit verbringst, im Stillen zu deinem Gott zu beten…*

* …verlierst du alle Zauber, die dir zuletzt gewährt wurden
* …werden dir neue Zauber deiner Wahl gewährt, die deine momentane Stufe nicht überschreiten und deren Gesamtstufe deine eigene Stufe+1 nicht überschreitet
* …bereitest du deine Gebete vor, die nicht gegen deine Obergrenze für Zauber zählen.

'''

*Zauber wirken*

'''
*Wenn du einen Zauber entfesselst, der dir von deiner Gottheit gewährt wurde,* würfle+WEI.
✴ Bei einer 10+ kann der Zauber seine Wirkung entfalten und deine Gottheit entzieht dir den Zauber nicht, so dass du ihn abermals wirken kannst.
✴ Bei einer 7–9 wirkt der Zauber ebenfalls, aber du wählst aus der folgenden Liste:

* Du ziehst ungewollt Aufmerksamkeit auf dich oder steckst in der Klemme.
Der SL wird dir beschreiben, wie.
* Das Wirken des Zaubers verstimmt deine Gottheit:
Nimm -1 fortlaufend auf _Zauber wirken_, bis du das nächste mal _in Gott gehst_.
* Nachdem du ihn gewirkt hast, entzieht deine Gottheit dir den Zauber.
Du kannst ihn nicht wirken, bis du das nächste mal _in Gott gehst_ und er dir gewährt wird.

Beachte, dass manche Zauber mit andauernden Effekten _Zauber wirken_ mit Abzügen belegen können.

'''

=== Gesinnung

*Wähle eine Gesinnung:*

==== Gut

Bringe dich selbst in Gefahr, um jemand anderen zu heilen.

==== Rechtschaffen

Bringe dich selbst in Gefahr, indem du den Geboten deiner Kirche oder deines Gottes folgst.

==== Böse

Bringe Leid über jemanden, um die Überlegenheit deiner Kirche oder deines Gottes unter Beweis zu stellen.

=== Ausrüstung

Deine Tragkraft beträgt 10+STR.
Du beginnst das Spiel mit Wegrationen _[5 Anwendungen]_ _[1 Gewicht]_ und einem heiligen Symbol – beschreibe es _[0 Gewicht]_.
Wähle deine Rüstung:

* Kettenpanzer _[1 Rüstung]_ _[1 Gewicht]_
* Schild _[+1 Rüstung]_ _[2 Gewicht]_

Wähle deine Waffe:

* Streithammer _[kurz]_ _[1 Gewicht]_
* Streitkolben _[kurz]_ _[1 Gewicht]_
* Stecken _[kurz]_ _[zweihändig]_ _[1 Gewicht]_ und Bandagen _[0 Gewicht]_

Wähle eins:

* Abenteurerausrüstung _[1 Gewicht]_ und Wegrationen _[5 Anwendungen]_ _[1 Gewicht]_
* Heiltrank _[0 Gewicht]_

=== Bande

Knüpfe Bande mit deinen Mitstreitern, indem du in mindestens eine der Leerstellen einen ihrer Namen einträgst:

pass:[____________] hat meine Gottheit beleidigt; ich traue ihm nicht.

pass:[____________] ist gut und fest im Glauben; ich vertraue ihm blind.

pass:[____________] schwebt in ständiger Gefahr; ich werde meine schützende Hand über ihn halten.

Ich arbeite daran, pass:[____________] zu meinem Glauben zu konvertieren.

=== Erweiterte Züge

*Wenn du eine Stufe von 2–5 erreichst, wähle aus den folgenden Zügen:*

*Auserwählt*

'''
Wähle einen Zauber.
Er wird dir gewährt, als wäre er eine Stufe niedriger.

'''

*Kräftigung*

'''
*Wenn du jemanden heilst,* nimmt er +2 voraus auf seinen Schaden.

'''

*Fürsprecher der Toten*

'''
*Wenn jemand in deiner Gegenwart seinen _letzten Atemzug_ nimmt,* erhält er +1 auf seinen Wurf.

'''

*Gleichmut*

'''
*Wenn du einen _Zauber wirkst_,* kannst du die erste -1-Strafe von andauernden Zaubern ignorieren.

'''

*Erste Hilfe*

'''
_Leichte Wunden heilen_ zählt als Gebet für dich – es zählt nicht mehr gegen die Obergrenze an Zauberstufen, die du gewährt bekommst.

'''

*Göttlicher Eingriff*

'''
*Wenn du _in Gott gehst_,* erhältst du 1 Reserve und verlierst Reserve, die du hattest.
*Wenn du oder einer deiner Gefährten Schaden nehmen,* kannst du deine Reserve ausgeben, um deinen Gott um Hilfe zu bitten.
Dein Gott greift mit einer angemessenen Geste (ein plötzlicher Windstoß, ein glücklicher Ausrutscher, das plötzliche Blenden der Sonne, die hinter den Wolken hervorbricht) in den Lauf der Dinge ein und verhindert den Schaden.

'''

*Büßer*

'''
*Wenn du Schaden nimmst und den Schmerz klaglos akzeptierst,* kannst du +1W4 Schaden _[ignoriert Rüstung]_ nehmen.
Wenn du das tust, erhältst du +1 voraus auf _Zauber wirken_.

'''

*Ermächtigen*

'''
*Wenn du _Zauber wirkst_,* hast du bei einer 10+ die Möglichkeit, von der 7–9-Liste zu wählen.
Wenn du das tust, darfst du dir zusätzlich einen der folgenden Effekte aussuchen:

* Die Zauber wirkt doppelt so stark
* Der Zauber betrifft doppelt so viele Ziele

'''

*Um Weisung ersuchen*

'''
*Wenn du deinem Gott etwas Wertvolles darbietest und ihn um Führung bittest,* wird er dir mitteilen, was für dich zu tun ist.
Wenn du ihm Folge leistest, schreibe dir 1 EP gut.

'''

*Göttlicher Schutz*

'''
*Wenn du keine Rüstung und keinen Schild trägst,* hast du 2 Rüstung.

'''

*Ergebener Heiler*

'''
*Wenn du jemand anderen von Schaden heilst,* kannst du deine Stufe zu den geheilten Trefferpunken hinzufügen.

'''

*Wenn du eine Stufe von 6–10 erreichst, wähle einen Zug aus der folgenden Liste oder der Liste der Stufe-2–5-Züge:*

*Gesalbt*

'''
_[erfordert_ Auserwählter__]__

Wähle einen weiteren Zauber.
Auch er wird dir gewährt, als wäre er eine Stufe niedriger.

'''

*Inbild der Götter*

'''
*Wenn du das erste mal nach Wahl dieses Zuges Zeit im Gebet verbringst,* wähle ein körperliches Merkmal, das mit deiner Gottheit in Verbindung steht (reißende Klauen, Flügel aus saphirnen Federn, ein allsehendes drittes Auge).
Nach dem Gebet wird dieses Merkmal ein immerwährender Teil von dir sein.

'''

*Schnitter*

'''
*Wenn du dir nach einem Gefecht Zeit nimmst, den Sieg deiner Gottheit zu widmen und dich um die Toten zu kümmern,* nimm +1 voraus.

'''

*Dem Schicksal ergeben*

'''
_[ersetzt_ Gleichmut__]__

*Wenn du einen _Zauber wirkst_,* kannst du die -1-Strafen von zwei andauernden Zaubern ignorieren.

'''

*Höhere erste Hilfe*

'''
_[benötigt_ Erste Hilfe__]__

_Mittelschwere Wunden heilen_ zählt als Gebet für dich – es zählt nicht mehr gegen die Obergrenze an Zauberstufen, die du gewährt bekommst.

'''

*Göttliche Unantastbarkeit*

'''
_[ersetzt_ Göttlicher Eingriff__]__

*Wenn du _in Gott gehst_,* erhältst du 2 Reserve und verlierst Reserve, die du hattest.
*Wenn du oder einer deiner Gefährten Schaden nehmen,* kannst du deine Reserve ausgeben, um deinen Gott um Hilfe zu bitten.
Dein Gott greift mit einer angemessenen Geste (ein plötzlicher Windstoß, ein glücklicher Ausrutscher, das plötzliche Blenden der Sonne, die hinter den Wolken hervorbricht) in den Lauf der Dinge ein und verhindert den Schaden.

'''

*Märtyrer*

'''
_[ersetzt_ Büßer__]__

*Wenn du Schaden nimmst und den Schmerz klaglos akzeptierst,* kannst du +1W4 Schaden _[ignoriert Rüstung]_ nehmen.
Wenn du das tust, erhältst du +1 voraus auf _Zauber wirken_ und addierst deine Stufe auf den Schaden oder die Heilwirkung des Zaubers.

'''

*Göttliche Rüstung*

'''
_[ersetzt_ Göttlicher Schutz__]__

*Wenn du keine Rüstung und keinen Schild trägst,* hast du 3 Rüstung.

'''

*Höheres Ermächtigen*

'''
_[ersetzt_ Ermächtigen__]__

*Wenn du _Zauber wirkst_,* hast du bei einer 10–11 die Möglichkeit, von der 7–9-Liste zu wählen.
Wenn du das tust, darfst du dir zusätzlich einen der folgenden Effekte aussuchen:

* Die Zauber wirkt doppelt so stark
* Der Zauber betrifft doppelt so viele Ziele

Bei einer 12+ darfst du einen der Effekte wählen, ohne von der 7–9-Liste wählen zu müssen.

'''

*Multitalent*

'''
Du erhältst einen Zug einer anderen Klasse.
Behandle deine Stufe dabei so, als sei sie um 1 niedriger.

'''
