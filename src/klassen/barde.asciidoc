== Der Barde

In den Liedern ist das Abenteurerleben voller weiter Straßen und erfüllt von Reichtum und Schlachten.
Und die Geschichten, die in jedem Gasthaus von den ansässigen Bauerntölpeln erzählt werden, enthalten doch sicher ein Körnchen Wahrheit, nicht wahr?
Was König und Knecht zugleich inspiriert, was das wilde Tier in uns beruhigt und gestandene Männer in die Raserei treibt, muss schließlich eine Quelle haben.

Vorhang auf für dich, Barde.
Überlieferst Geschichten und singst deine Lieder, mit süßer Zunge und scharfem Witz.
Ein bloßer Spielmann kann von einer Sache künden, doch nur du hast sie gelebt.
Schnür die Stiefel fest, edler Redner;
schärf den Dolch und folge dem Ruf.
Einer muss Zeuge sein und Kopf an Kopf mit den Schlägern stehen, mit windigen Strolchen und Helden _in spe_.
Und wer von ihnen ist besser geeignet als du, um Geschichte zu schreiben?

Niemand.
Mach dich auf den Weg.

=== Namen

_Elf:_ Astrafel, Daelwyn, Feliana, Damarra, Sistranalle, Pendrell, Melliandre, Dagoliir

_Mensch:_ Baldric, Leena, Dunwick, Willem, Edwyn, Florian, Seraphine, Quorra, Charlotte, Lily, Ramonde, Cassandra

=== Aussehen

Wähle von jeden eins:

Wissende, feurige oder freudige Augen

Raffinierte Frisur, stürmisches Haar oder stilvoller Hut

Prachtvolle Kleidung, Reisekleidung oder Lumpen

Drahtig, wohlgenährt oder schlank

=== Attribute

Deine maximalen TP sind 6+Konstitution.

Dein Grundschaden beträgt 1W6.

=== Anfangszüge

*Wähle eine Rasse und erlange ihren Zug:*

*Elf*

'''
*Wenn du einen bedeutenden Ort betrittst,* darfst du den SL nach einem Fakt aus seiner Geschichte fragen.
Ob ein Ort bedeutend ist oder nicht, ist deine Entscheidung.

'''

*Mensch*

'''
*Wenn du zum ersten mal einen zivilisierten Ort betrittst,* wird jemand, der das Hausgebot für fahrende Sänger ehrt, dir ein Dach über dem Kopf anbieten.

'''

*Du beginnst das Spiel mit den folgenden Zügen:*

*Zauberkunst*

'''
*Wenn du eine Vorführung in einen einfachen Zauber webst,* wähle einen Verbündeten und einen Effekt:

* Heile 1W8 Schaden
* +1W4 voraus auf den Schaden
* Der Geist wird von einem Zauber befreit
* Wenn ihm jemand das nächste mal erfolgreich _hilft_, erhält dein Verbündeter +2 statt +1

Dann würfle+CHA:

✴ Bei einer 10+ erlangt dein Verbündeter den gewählten Effekt.
✴ Bei einer 7–9 tritt der gewünschte Effekt ein, aber du ziehst ungewollte Aufmerksamkeit auf dich, oder ein Echo des Zaubers wirkt auf andere Wesen – die Entscheidung liegt beim SL.

'''

*Sagenkunde*

'''
Wähle dein Fachgebiet:

* Zauber und Magie
* Die Toten und Untoten
* Die großen Historien der bekannten Welt
* Ein Bestiarium unerhörter Kreaturen
* Die Planaren Sphären
* Legenden vergangener Helden
* Götter und ihre Diener

*Wenn du einer einer Kreatur von Bedeutung, einem Ort mit Geschichte oder einem Gegenstand aus den Erzählungen zum ersten mal über den Weg läufst und dich auf Grund deines Fachgebiets damit auskennst,* kannst du dem SL eine Frage darüber stellen, die er wahrheitsgemäß beantworten muss.
Der SL kann dich dann fragen, auf welcher Sage, welchem Lied oder welcher Legende dein Wissen fußt.
Welche Wesen, Orte oder Gegenstände bedeutend sind, ist deine Entscheidung.

'''

*Charmant und Offen*

'''
*Wenn du freiheraus mit jemandem redest,* kannst du seinem Spieler eine der Fragen von der folgenden Liste stellen.
Er muss sie wahrheitsgemäß beantworten und darf dir danach selbst eine Frage stellen, die du ebenfalls mit der Wahrheit beantworten musst.

* Wem dienst du?
* Was, wünschst du, soll ich tun?
* Wie bringe ich dich dazu, pass:[____________]?
* Was geht gerade wirklich in dir vor?
* Was begehrst du am meisten?

'''

*Ein Hafen im Sturm*

'''
*Wenn du an einen zivilisierten Ort zurückkehrst, den du bereits besucht hast,* sage dem SL, wann du zuletzt hier warst.
Er wird dir erzählen, wie sich der Ort seitdem verändert hat.

'''

=== Gesinnung

*Wähle eine Gesinnung:*

==== Gut

Deine Kunst dient dem Wohle anderer.

==== Neutral

Vermeide einen Konflikt oder entschärfe eine brenzlige Situation.

==== Chaotisch

Sporne andere an, spontan und rückhaltlos zu handeln.

=== Ausrüstung

Deine Tragkraft beträgt 9+STR.
Du beginnst das Spiel mit Wegrationen _[5 Anwendungen]_ _[1 Gewicht]_.
Wähle ein Instrument, alle haben _[0 Gewicht]_ für dich.

* Die Mandoline deines Vaters, repariert
* Eine feine Laute, das Geschenk eines Adligen.
* Die Flöte, mit der du deiner erste Liebe den Hof machtest
* Ein gestohlenes Horn
* Eine Geige, nie gespielt
* Ein Gesangbuch in einer vergessenen Sprache

Wähle deine Kleidung:

* Lederrüstung _[1 Rüstung]_ _[1 Gewicht]_
* Pompöse Kleider _[0 Gewicht]_

Wähle deine Waffen:

* Duelldegen _[kurz]_ _[präzise]_ _[2 Gewicht]_
* Abgegriffener Bogen _[nah]_ _[2 Gewicht]_, Bündel Pfeile _[3 Munition]_ _[1 Gewicht]_ und Kurzschwert _[kurz]_ _[1 Gewicht]_

Wähle außerdem ins:

* Abenteurerausrüstung _[1 Gewicht]_
* Bandagen _[0 Gewicht]_
* Halblingsches Pfeifenkraut _[0 Gewicht]_
* 3 Münzen

=== Bande

Knüpfe Bande mit deinen Mitstreitern, indem du in mindestens eine der Leerstellen einen ihrer Namen einträgst:

Dies ist nicht mein erstes Abenteurer mit pass:[____________].

Ich habe Lieder über pass:[____________] gesungen, lange, bevor ich ihm gegenüberstand.

pass:[____________] ist oft Ziel meiner Witze.

Ich schreibe eine Ballade über die Abenteuer von pass:[____________].

pass:[____________] hat mir ein Geheimnis anvertraut.

pass:[____________] vertraut mir nicht – und zwar aus gutem Grund.

=== Erweiterte Züge

*Wenn du eine Stufe von 2–5 erreichst, wähle aus den folgenden Zügen:*

*Heilendes Lied*

'''
*Wenn du mit _Zauberkunst_ heilst,* dann heilst du +1W8 Trefferpunkte.

'''

*Wildes Aufspiel*

'''
*Wenn Du _Zauberkunst_ benutzt, um Bonusschaden zu gewähren,* bekommt dein Ziel zusätzliche +1W4 Schaden.

'''

*Unser Marschall geht bis elf*

'''
*Wenn du einen wahnsinnigen Auftritt hinlegst (ein redliches Flötensolo oder einen mächtigen Fanfarenstoß),* wähle ein Ziel in Hörweite und würfle+CHA.
✴ Bei einer 10+ greift das Ziel seinen nächsten Verbündeten in Reichweite an.
✴ Bei einer 7–9 greift das Ziel seinen nächsten Verbündeten an, aber du ziehst auch seine Aufmerksamkeit auf dich – und seinen Zorn.

'''

*Wahres Metall*

'''
*Wenn dein Schrei Trommelfelle zerschmettern kann oder dein Instrument ohrenbetäubend laut spielt,* wähle ein Ziel und würfle+KON.
✴ Bei einer 10+ nimmt das Ziel 1W10 Schaden und ist für ein paar Minuten taub.
✴ Bei einer 7–9 fügst du ebenfalls Schaden zu, aber die Sache gerät außer Kontrolle:
Der SL wählt ein zusätzliches Ziel in der Nähe.

'''

*Ein wenig Hilfe von meinen Freunden*

'''
*Wenn du jemanden erfolgreich _hilfst_,* bekommst du ebenfalls +1 voraus.

'''

*Kosmischer Klang*

'''
Die _Zauberkunst_ in dir ist stark, was es dir erlaubt, zwei Effekte statt einem zu wählen.

'''

*Duellantenparade*

'''
*Beim _Hauen und Stechen_* erhältst du +1 Rüstung voraus.

'''

*Schwindel*

'''
*Wenn du mit jemanden _schacherst_,* bekommst du bei einer 7+ zusätzlich +1 voraus gegen ihn.

'''

*Hansdampf in allen Gassen*

'''
Du erhältst einen Zug einer anderen Klasse.
Behandle deine Stufe dabei so, als sei sie um 1 niedriger.

'''

*Träger vieler Hüte*

'''
Du erhältst einen Zug einer anderen Klasse.
Behandle deine Stufe dabei so, als sei sie um 1 niedriger.

'''

*Wenn du eine Stufe von 6–10 erreichst, wähle einen Zug aus der folgenden Liste oder der Liste der Stufe-2–5-Züge:*

*Heilender Chor*

'''
Ersetzt _Heilendes Lied_

*Wenn du mit _Zauberkunst_ heilst,* dann heilst du +2W8 Trefferpunkte.

'''

*Entfesseltes Aufspiel*

'''
Ersetzt _Wildes Aufspiel_

*Wenn Du _Zauberkunst_ benutzt, um Bonusschaden zu gewähren,* bekommt dein Ziel zusätzliche +2W4 Schaden.

'''

*Unvergessliches Gesicht*

'''
*Wenn du jemanden nach einiger Zeit wiedertriffst (deine Entscheidung),* bekommst du +1 voraus gegen ihn.

'''

*Vorauseilender Ruf*

'''
*Wenn du zum ersten mal jemanden triffst, der Lieder über dich gehört hat,* würle+CHA.
✴ Bei einer 10+ darfst du dem SL zwei Dinge sagen, die er über dich gehört hat.
✴ Bei einer 7–9 bestimmst du die eine Sache, der SL die zweite.

'''

*Kosmischer Gleichklang*

'''
Ersetzt: _Kosmischer Klang_

*Wenn du deine _Zauberkunst_ benutzt,* darfst du zwei Effekte wählen.
Du kannst außerdem einen der Effekte verdoppeln.

'''

*Ein Gehör für Magie*

'''
*Wenn du hörst, wie ein Feind Magie wirkt,* wird der SL dir den Namen des Zaubers und seine Effekte mitteilen.
Nimm +1 voraus, wenn du dir diese Erkenntnis zu Nutze machst.

'''

*Verschlagen*

'''
*Wenn du _Charmant und Offen_ benutzt,* darfst du außerdem fragen: „Wie kann ich dich an deiner verletzlichsten Stelle treffen?"
Der Gefragte wird diese Frage nicht an dich zurückstellen.

*Duellantenblock*

'''
Ersetzt _Duellantenparade_

*Beim _Hauen und Stechen_* erhältst du +2 Rüstung voraus.

'''

*Betrug*

'''
Ersetzt: _Schwindel_

*Wenn du mit jemanden _schacherst_,* erhältst du bei einer 7+ zusätzlich +1 voraus gegen ihn.
Der Spieler deines Verhandlungspartners muss außerdem eine deiner Fragen wahrheitsgemäß beantworten.

'''

*Meister aller Klassen*

'''
Du erhältst einen Zug einer anderen Klasse.
Behandle deine Stufe dabei so, als sei sie um 1 niedriger.

'''
